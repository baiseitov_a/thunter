$(function(){

  $('.js-select').styler();

  $('.armd-nav__item').click(function(){
    var $this = $(this);
    $this.toggleClass('active');
    var $parent = $this.parents('li');
    if ($parent.hasClass('dropdown')) {
      $this.siblings('.armd-nav__sub').slideToggle()
      return false
    }
  })

  $('.js-dropdawn').click(function(){
    $(this).find('.dropdawn').slideToggle();
  })

  $('.popup-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,

    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom'
  });

  // параллакс эффект
  if ($('.shadow-title').length){
    var rellax = new Rellax('.shadow-title', {
      center: true,
      speed: -2
    });
  }

  $('.tab-link').click(function(){
    $('.tab-link').removeClass('active');
    $(this).addClass('active');
    var id = $(this).attr('href');
    $('.tab-item').removeClass('active');
    $(id).addClass('active');
    return false
  })
  
  // мобайл меню
  $('#nav-icon1').click(function () {
    $(this).toggleClass('open');
    $('.header-mobile-menu').slideToggle();
  });

  // анимация чисел на главной

  if ($('.count').length){
    $('.count').each(function () {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
          duration: 6000,
          easing: 'swing',
          step: function (now) {
            $(this).text(Math.ceil(now));
          }
        });
    });
  }

  if ($('.js-anim').length)  $('.js-anim').viewportChecker();


  /****** ***Ввод в инпут только цифр ***********/

  $('.js-number').keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  })

  /****** ****Ввод в инпут только букв ***********/
  if ($('.js-phone').length) $('.js-letter').inputmask('Regex', { regex: '^[а-яА-Яa-zA-Z ]*$' });  

  /*********** Маска для телефона **************/
  if ($('.js-phone').length ) $('.js-phone').inputmask("+7(999)999-99-99");


  // ParticlesJS Config.

  if($('#particles-js').length){
    
    particlesJS("particles-js", {
      "particles": {
        "number": {
          "value": 80,
          "density": {
            "enable": true,
            "value_area": 700
          }
        },
        "color": {
          "value": "#646463"
        },
        "shape": {
          "type": "circle",
          "stroke": {
            "width": 0,
            "color": "#000000"
          },
          "polygon": {
            "nb_sides": 5
          },
        },
        "opacity": {
          "value": 0.5,
          "random": false,
          "anim": {
            "enable": false,
            "speed": 1,
            "opacity_min": 0.1,
            "sync": false
          }
        },
        "size": {
          "value": 3,
          "random": true,
          "anim": {
            "enable": false,
            "speed": 40,
            "size_min": 0.1,
            "sync": false
          }
        },
        "line_linked": {
          "enable": true,
          "distance": 150,
          "color": "#646463",
          "opacity": 0.4,
          "width": 1
        },
        "move": {
          "enable": true,
          "speed": 6,
          "direction": "none",
          "random": false,
          "straight": false,
          "out_mode": "out",
          "bounce": false,
          "attract": {
            "enable": false,
            "rotateX": 600,
            "rotateY": 1200
          }
        }
      },
      "interactivity": {
        "detect_on": "canvas",
        "events": {
          "onhover": {
            "enable": true,
            "mode": "grab"
          },
          "onclick": {
            "enable": true,
            "mode": "push"
          },
          "resize": true
        },
        "modes": {
          "grab": {
            "distance": 140,
            "line_linked": {
              "opacity": 1
            }
          },
          "bubble": {
            "distance": 400,
            "size": 40,
            "duration": 2,
            "opacity": 8,
            "speed": 3
          },
          "repulse": {
            "distance": 200,
            "duration": 0.4
          },
          "push": {
            "particles_nb": 4
          },
          "remove": {
            "particles_nb": 2
          }
        }
      },
      "retina_detect": true
    });

  }
})