'use strict';

var gulp = require('gulp'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    notify      = require('gulp-notify'),
    wait = require('gulp-wait'),
    cache = require('gulp-cache'),
    concat = require('gulp-concat'),
    rename = require("gulp-rename"),
    iconfont = require("gulp-iconfont"),
    iconfontCss = require("gulp-iconfont-css");


var path = {
    build: {
        html: 'build/html/',
        js: 'build/scripts/',
        css: 'build/css/',
        img: 'build/images/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/jade/**/*.jade',       
        js: 'src/scripts/*.js',
        style: 'src/sass/**/*.*',
        img: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },   

    watch: {
        html: 'src/jade/**/*.jade',
        js: 'src/scripts/**/*.js',
        style: 'src/sass/**/*.*',
        img: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: [ "./", "./build"]
        
    },
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Aidar"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html) 
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) 
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('concat-js', function () {
    return gulp.src([
        'vendor/jquery/dist/jquery.min.js',
        'vendor/bootstrap/dist/js/bootstrap.min.js',
        'vendor/particles.js/particles.min.js',
        'vendor/jQuery-viewport-checker/dist/jquery.viewportchecker.min.js',
        'vendor/inputmask/jquery.inputmask.bundle.min.js',
        'vendor/air-datepicker/dist/js/datepicker.min.js',
        'vendor/jquery.form-styler/dist/jquery.formstyler.min.js',
        'vendor/rellax/rellax.min.js',
        'vendor/magnific-popup/dist/jquery.magnific-popup.min.js'
    ])
        .pipe(concat('libs.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(wait()) 
        .pipe(sass({ errLogToConsole: true }).on('error', notify.onError({
            message: "Error: <%= error.message %>",
            title  : "Styles!"
        } ) ))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) 
        .pipe(cache(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        })))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'concat-js',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});


gulp.task('iconfont', function(){
    gulp.src(['src/images/svg-iconfont/**/*.svg'])
        .pipe(iconfontCss({
            fontName: 'iconic',
            path: 'src/sass/fonts/_iconfont-template.scss',
            targetPath: '../sass/fonts/_iconfont.scss',
            fontPath: '../fonts/',
            cssClass: 'iconic'
        }))
        .pipe(iconfont({
            fontName: 'iconic',
            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
            startUnicode: true,
            prependUnicode: true,
            normalize: true,
            fontHeight: 1001,
            centerHorizontally: true
        }))
        .pipe(
            gulp.dest('src/fonts')
        );
});



gulp.task('default', ['build', 'webserver', 'watch']);